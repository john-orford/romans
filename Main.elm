module Main exposing (..)

import Parser exposing (..)
        
i = keep (Exactly 1) (\c -> c == 'I')
    
v = keep (Exactly 1) (\c -> c == 'V')
      
x = keep (Exactly 1) (\c -> c == 'X')
    
l = keep (Exactly 1) (\c -> c == 'L')
    
c = keep (Exactly 1) (\c -> c == 'C')
    
d = keep (Exactly 1) (\c -> c == 'D')
    
m = keep (Exactly 1) (\c -> c == 'M')
    
iv = succeed (++)
     |= i
     |= v

ix = succeed (++)
     |= i
     |= x

xl = succeed (++)
     |= x
     |= l
        
xc = succeed (++)
     |= x
     |= c
        
cd = succeed (++)
     |= c
     |= d

cm = succeed (++)
     |= c
     |= m
        
parse = [try iv, try ix, try xl, try xc, try cd, try cm, i, v, x, l, c, d, m]
     |> Parser.oneOf 
     |> repeat oneOrMore 

eval x =
    case x of
        Ok rs -> Ok ( List.foldl sum 0 rs )
        Err e -> Err e

fromRoman s = s |> run parse |> eval 
                 
-- better to use ADT instead of stringy-goopiness                 
type RN = IV | IX | XL | XC | CD | CM | I | V | X | L | C | D | M
                 
sum r a = a + case r of
                   "IV" -> 4
                   "IX" -> 9
                   "XL" -> 40
                   "XC" -> 90
                   "CD" -> 400
                   "CM" -> 900
                   "I" -> 1
                   "V" -> 5
                   "X" -> 10
                   "L" -> 50
                   "C" -> 100
                   "D" -> 500
                   "M" -> 1000
                   _ -> 0
                 
try : Parser a -> Parser a
try parser =
  delayedCommitMap always parser (succeed ())
     


-- convert from arabic to roman
-- https://www.rapidtables.com/convert/number/how-number-to-roman-numerals.html

-- A dictionary storing character & numbers would avoid this unpleasantness..

toRoman_ x = toRoman x [] |> List.reverse |> String.concat
toRoman x a = if x // 1000 > 0 then
                  toRoman (x - 1000) a ++ ["M"]
              else if x // 900 > 0 then
                       toRoman (x - 900) a ++ ["CM"]
                   else if x // 500 > 0 then
                            toRoman (x - 500) a ++ ["D"]
                        else if x // 400 > 0 then
                                 toRoman (x - 400) a ++ ["CD"]
                             else if x // 100 > 0  then
                                      toRoman (x - 100) a ++ ["C"]
                                  else if x // 90 > 0  then
                                           toRoman (x - 90) a ++ ["XC"]
                                       else if x // 50 > 0  then
                                                toRoman (x - 50) a ++ ["L"]
                                            else if x // 40 > 0  then
                                                     toRoman (x - 40) a ++ ["XL"]
                                                 else if x // 10 > 0  then
                                                          toRoman (x - 10) a ++ ["X"]
                                                      else if x // 9 > 0  then
                                                               toRoman (x - 9) a ++ ["IX"]
                                                           else if x // 5 > 0  then
                                                                    toRoman (x - 5) a ++ ["V"]
                                                                else if x // 4 > 0  then
                                                                         toRoman (x - 4) a ++ ["IV"]
                                                                     else if x // 1 > 0  then
                                                                              toRoman (x - 1) a ++ ["I"]
                                                                          else
                                                                              a



        

                                                                                  
