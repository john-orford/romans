# Encode & Decode Roman Numerals


## Preparation

```
elm-package install;
elm-repl;
```

## Sanity Check

```
> import Main
> Main.fromRoman "MMIX"
Ok 2009 : Result.Result Parser.Error number
> > Main.toRoman_ 1989
"MCMLXXXIX" : String
```

## Tests

```
elm-test # on the command line
```
