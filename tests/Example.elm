module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Main exposing (..)

---
-- Tests

suite : Test
suite =
    describe "The Roman Numeral encode / decode module"
            [
            -- fuzz runs the test 100 times with randomly-generated inputs!
            fuzz range "restores the original integer" <|
                \randomlyGeneratedInt ->
                    randomlyGeneratedInt
                        |> toRoman_
                        |> fromRoman
                        |> Expect.equal (Ok randomlyGeneratedInt)
            ]


range = Fuzz.intRange 1 1000000
